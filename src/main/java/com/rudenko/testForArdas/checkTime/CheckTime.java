package com.rudenko.testForArdas.checkTime;

import org.apache.log4j.Logger;

import java.util.*;


/**
 * Class for checking system time and checking wich part of the day now
 * <p/>
 * Created by Evgen on 15.12.2015.
 */
public class CheckTime {


    /**
     * Create logger
     */
    private static final Logger log = Logger.getLogger(CheckTime.class);

    /**
     * Localization of the operating system
     */
    Locale defaultLocale = Locale.getDefault();

    /**
     * An indication to resource for the text messages
     */
    ResourceBundle bundle_def = ResourceBundle.getBundle("messages", defaultLocale);


    /**
     * Variable indicates the hour begins the morning
     */
    private int morning = 6;

    /**
     * Variable indicates the hour begins the day
     */
    private int day = 9;

    /**
     * Variable indicates the hour begins the evening
     */
    private int evening = 19;

    /**
     * Variable indicates the hour begins the night
     */
    private int night = 23;

    /**
     * Method for obtaining the current hour
     *
     * @return current hour (int)
     */
    public int checkTime() {
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.getTime();
        log.info(bundle_def.getString("checkTime_succes"));
        return currentCalendar.get(Calendar.HOUR_OF_DAY);
    }

    /**
     * Method determines the part of day and welcomes the user accordingly
     */
    public void setPartOfDay(int currentHour) {

        log.info(bundle_def.getString("startCheck_timeDay"));

        if (currentHour == night || currentHour < morning) {
            log.info(bundle_def.getString("night_now"));
            System.out.println(bundle_def.getString("good_night"));
        }

        if (currentHour >= morning && currentHour < day) {
            log.info(bundle_def.getString("morning_now"));
            System.out.println(bundle_def.getString("good_morning"));
        }

        if (currentHour >= day && currentHour < evening) {
            log.info(bundle_def.getString("day_now"));
            System.out.println(bundle_def.getString("good_day"));
        }

        if (currentHour >= evening && currentHour < night) {
            log.info(bundle_def.getString("evening_now"));
            System.out.println(bundle_def.getString("good_evening"));
        }
        if (currentHour < 0 || currentHour > night){
            log.error(bundle_def.getString("invalid_time"));
            System.out.println(bundle_def.getString("invalid_time"));
        }

        log.info(bundle_def.getString("endCheck_timeDay"));

    }


    /**
     * Return the hour, when starting morning
     *
     * @return the hour, when starting morning
     */
    public int getMorning() {
        return morning;
    }

    /**
     * Sets the hour, when starting morning
     *
     * @param morning - time of starting morning
     */
    public void setMorning(int morning) {
        this.morning = morning;
    }

    /**
     * Return the hour, when starting day
     *
     * @return the hour, when starting day
     */
    public int getDay() {
        return day;
    }

    /**
     * Sets the hour, when starting day
     *
     * @param day - time of starting day
     */
    public void setDay(int day) {
        this.day = day;
    }

    /**
     * Return the hour, when starting night
     *
     * @return the hour, when starting night
     */
    public int getNight() {
        return night;
    }

    /**
     * Sets the hour, when starting night
     *
     * @param night - time of starting night
     */
    public void setNight(int night) {
        this.night = night;
    }

    /**
     * Return the hour, when starting evening
     *
     * @return the hour, when starting evening
     */
    public int getEvening() {
        return evening;
    }

    /**
     * Sets the hour, when starting evening
     *
     * @param evening - time of starting evening
     */
    public void setEvening(int evening) {
        this.evening = evening;
    }
}
