package com.rudenko.testForArdas;

import com.rudenko.testForArdas.checkTime.CheckTime;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.util.Locale;
import java.util.ResourceBundle;


/**
 * Application entry point
 */
public class Main {

    /**
     * Create logger
     */
    private static final Logger log = Logger.getLogger(Main.class);

    public static void main(String[] args) throws ParseException {

        /**
         * Localization of the operating system
         */
        Locale defaultLocale = Locale.getDefault();

        /**
         * An indication to resource for the text messages
         */
        ResourceBundle bundle_def = ResourceBundle.getBundle("messages", defaultLocale);

        log.info(bundle_def.getString("start_app"));

        /**
         * Creating an object class Checktime
         */
        CheckTime checkTimeObj = new CheckTime();

        /**
         * Determining the current time
         */
        int currentTime = checkTimeObj.checkTime();

        /**
         * Determining the part of day and welcomes the user
         */
        checkTimeObj.setPartOfDay(currentTime);

        log.info(bundle_def.getString("end_app"));

    }
}
